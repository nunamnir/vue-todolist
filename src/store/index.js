import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    notes: [
      //раскоментируй чтобы увидеть стартовую заметку
      // {
      //   title: "Path of happiness",
      //   tasks: [
      //     {
      //       done: true,
      //       description: 'Plant a tree',
      //       oldDescription: '',
      //       newDescription: ''
      //     },
      //     {
      //       done: false,
      //       description: 'Build a house',
      //       oldDescription: '',
      //       newDescription: ''
      //     },
      //     {
      //       done: false,
      //       description: 'Make a family',
      //       oldDescription: '',
      //       newDescription: ''
      //     },
      //   ]
      // }
    ]
  },
  mutations: {
    refreshPage(state) {
      if(localStorage.currentNotes){
        state.notes = JSON.parse(localStorage.getItem('currentNotes'));
      }
    },
    // сделать функцию инициализации oldDescription
    // setStepBackInput(state, values){
    //   state.stepBackInputs.splice(values.index, 1, values.oldStr);
    // },
    // setStepForwardInput(state, values){
    //   state.stepForwardInputs.splice(values.index, 1, values.newStr);
    // },
    addNote(state, obj) {      
      state.notes.push(obj);
      localStorage.setItem('currentNotes', JSON.stringify(state.notes));
    },
    changeNote(state, newObj){
      state.notes.splice(newObj.id, 1, newObj);
      localStorage.setItem('currentNotes', JSON.stringify(state.notes));
    },
    deleteNote(state, index){
      state.notes.splice(index, 1);
      localStorage.setItem('currentNotes', JSON.stringify(state.notes));
    },
    deleteAllNotes(state){
      state.notes = [];
      // state.notes.splice(0, state.notes.length);
      localStorage.setItem('currentNotes', JSON.stringify(state.notes));
      localStorage.clear();
    },
  },
  actions: {
    refreshPage: (context) => {
      context.commit('refreshPage');
    },
    // setStepsInputs: (context, values) => {
    //   context.commit('setStepBackInput', values);
    //   context.commit('setStepForwardInput', values);
    // },
    addNote: (context, obj) => {
      context.commit('addNote', obj);
    },
    changeNote: (context, newObj) => {     
      context.commit('changeNote', newObj);
    },
    deleteNote: (context, index) => {
      context.commit('deleteNote', index);
    },
    deleteAllNotes: (context) => {
      context.commit('deleteAllNotes');
    }
  },
  getters: {
    notes(state) {
      return state.notes;
    }
  }
})
